﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CycleAnalysisSoftware
{
    public partial class Form1 : Form
    {

        string time;
        string date;
        string maxhr;
        string heartrate1;
        string speed1;
        string altitude1;
        string power1;
        string cadence1;
        string[] cycleinfo;
        string linereader;



        public Form1()
        {
            InitializeComponent();

            string line;






            // Create an OpenFileDialog object.
            OpenFileDialog openFile1 = new OpenFileDialog();

            // Initialize the filter to look for text files.
            openFile1.Filter = "All Files (*.*)|*.*|Text Files|*.txt";

            // If the user selected a file, load its contents into the RichTextBox. 
            if (openFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    dataGridView1.Columns.Add("date", "Date");
                    dataGridView1.Columns.Add("time", "Time");
                    dataGridView1.Columns.Add("hr", "HR (BPM)");
                    dataGridView1.Columns.Add("speed", "Speed");
                    dataGridView1.Columns.Add("cadence", "Cadence");
                    dataGridView1.Columns.Add("altitude", "Altitude");
                    dataGridView1.Columns.Add("power", "Power");

                    StreamReader myfilereader = new StreamReader(openFile1.FileName);

                    while ((line = myfilereader.ReadLine()) != null)
                    {

                        if (line.Contains("Length"))
                        {
                            time = line.Substring(line.IndexOf("=") + 1);
                            label5.Text = time;
                        }

                        else if (line.Contains("Date"))
                        {
                            date = line.Substring(line.IndexOf("=") + 1);
                            label3.Text = date;
                        }

                        else if (line.Contains("MaxHR"))
                        {
                            maxhr = line.Substring(line.IndexOf("=") + 1);
                            label15.Text = maxhr;
                        }



                        else if (line.Contains("[HRData]"))
                        {
                            int i = 0;
                            while ((linereader = myfilereader.ReadLine()) != null)
                            {



                                cycleinfo = linereader.Split('\t');


                                heartrate1 = cycleinfo[0];
                                speed1 = cycleinfo[1];
                                cadence1 = cycleinfo[2];
                                altitude1 = cycleinfo[3];
                                power1 = cycleinfo[4];

                                this.dataGridView1.Rows.Add();
                                this.dataGridView1.Rows[i].Cells[0].Value = date;
                                this.dataGridView1.Rows[i].Cells[1].Value = time;
                                this.dataGridView1.Rows[i].Cells[2].Value = heartrate1;
                                this.dataGridView1.Rows[i].Cells[3].Value = speed1;
                                this.dataGridView1.Rows[i].Cells[4].Value = cadence1;
                                this.dataGridView1.Rows[i].Cells[5].Value = altitude1;
                                this.dataGridView1.Rows[i].Cells[6].Value = power1;

                                //dataGridView1.Rows.Add(cycleinfo);

                                i = i + 1;
                            }

                        }

                    }
                }
                catch (IOException e)
                {
                    Console.WriteLine(e);


            } 
            }







        }
    }
}

