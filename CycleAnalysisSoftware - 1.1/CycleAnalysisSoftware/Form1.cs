﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CycleAnalysisSoftware
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();


            // Create an OpenFileDialog object.
            OpenFileDialog openFile1 = new OpenFileDialog();

            // Initialize the filter to look for text files.
            openFile1.Filter = "Text Files|*.txt|All Files (*.*)|*.*";

            // If the user selected a file, load its contents into the RichTextBox. 
            if (openFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                richTextBox1.LoadFile(openFile1.FileName,
                RichTextBoxStreamType.PlainText);

            StreamReader objstream = new StreamReader(openFile1.FileName);

            string[] lines = objstream.ReadToEnd().Split(new char[] { '\n' });
            
           
            
            label3.Text = lines[4];

            label5.Text = lines[5];

            var date = label3.Text;
            var dateout = date.Substring(date.IndexOf("=") + 1).Trim();
            label3.Text = dateout;

            var time = label5.Text;
            var timeout = time.Substring(time.IndexOf("=") + 1).Trim();
            label5.Text = timeout;

        }


    }
}

