﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CycleAnalysisSoftware
{
    public partial class Form1 : Form
    {

        string time;
        string date;
        string maxhr;


        public Form1()
        {
            InitializeComponent();

            string line;

            // Create an OpenFileDialog object.
            OpenFileDialog openFile1 = new OpenFileDialog();

            // Initialize the filter to look for text files.
            openFile1.Filter = "All Files (*.*)|*.*|Text Files|*.txt";

            // If the user selected a file, load its contents into the RichTextBox. 
            if (openFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {


                StreamReader myfilereader = new StreamReader(openFile1.FileName);

                while ((line = myfilereader.ReadLine()) != null)
                {

                    if (line.Contains("Length"))
                    {
                        time = line.Substring(line.IndexOf("=") + 1);
                        label5.Text = time;
                    }

                    else if (line.Contains("Date"))
                    {
                        date = line.Substring(line.IndexOf("=") + 1);
                        label3.Text = date;
                    }

                    else if (line.Contains("MaxHR"))
                    {
                        maxhr = line.Substring(line.IndexOf("=") + 1);
                        label15.Text = maxhr;
                    }

                }
            }
            

        }



        
    }
}

