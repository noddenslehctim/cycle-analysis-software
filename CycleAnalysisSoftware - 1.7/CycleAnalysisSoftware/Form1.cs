﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CycleAnalysisSoftware
{
    public partial class Form1 : Form
    {
        string legnth;
        string time;
        string date;
        string interval;
        string maxhr;
        string heartrate1;
        string speed1;
        string altitude1;
        string power1;
        string cadence1;
        string[] cycleinfo;
        string[] timesplit;
        string linereader;
        string hours;
        string minutes;
        string seconds;
        int i = 0;
        int rows = 0;
        int avghr = 0;
        int avgaltitude = 0;
        int avgpower = 0;
        int tothr = 0;
        int maxheartrateint = 0;
        int minheartrateint = 1000;
        int maxpowerint = 0;
        int maxaltitudeint = 0;
        int totaltitude = 0;
        int totpower = 0;
        double maxspeedint = 0;
        double hourdis = 0;
        double mindis = 0;
        double secdis = 0;
        double minutespeed = 0;
        double secondspeed = 0;
        double totaldistance = 0;
        double totspeed = 0;
        double avgspeed = 0;



        public Form1()
        {
            InitializeComponent();

            string line;






            // Create an OpenFileDialog object.
            OpenFileDialog openFile1 = new OpenFileDialog();

            // Initialize the filter to look for text files.
            openFile1.Filter = "All Files (*.*)|*.*|Text Files|*.txt";

            // If the user selected a file, load its contents into the RichTextBox. 
            if (openFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    dataGridView1.Columns.Add("date", "Date");
                    dataGridView1.Columns.Add("time", "Time");
                    dataGridView1.Columns.Add("hr", "HR (BPM)");
                    dataGridView1.Columns.Add("speed", "Speed");
                    dataGridView1.Columns.Add("cadence", "Cadence");
                    dataGridView1.Columns.Add("altitude", "Altitude");
                    dataGridView1.Columns.Add("power", "Power");

                    StreamReader myfilereader = new StreamReader(openFile1.FileName);

                    while ((line = myfilereader.ReadLine()) != null)
                    {

                        if (line.Contains("Length"))
                        {
                            legnth = line.Substring(line.IndexOf("=") + 1);

                            time = line.Substring(line.IndexOf("=") + 1);
                            label5.Text = time;

                            timesplit = legnth.Split(':');

                            hours = timesplit[0];
                            minutes = timesplit[1];
                            seconds = timesplit[2];


                        }


                        else if (line.Contains("Date"))
                        {
                            date = line.Substring(line.IndexOf("=") + 1);
                            label3.Text = date;
                        }

                        else if (line.Contains("Interval"))
                        {
                            interval = line.Substring(line.IndexOf("=") + 1);
                            label27.Text = interval;
                        }


                        else if (line.Contains("[HRData]"))
                        {
                            

                            while ((linereader = myfilereader.ReadLine()) != null)
                            {



                                cycleinfo = linereader.Split('\t');


                                heartrate1 = cycleinfo[0];
                                speed1 = cycleinfo[1];
                                cadence1 = cycleinfo[2];
                                altitude1 = cycleinfo[3];
                                power1 = cycleinfo[4];

                                int speedint = Int32.Parse(speed1);
                                double formattedspeed = (double)speedint /  10;


                                this.dataGridView1.Rows.Add();
                                this.dataGridView1.Rows[i].Cells[0].Value = date;
                                this.dataGridView1.Rows[i].Cells[1].Value = time;
                                this.dataGridView1.Rows[i].Cells[2].Value = heartrate1;
                                this.dataGridView1.Rows[i].Cells[3].Value = formattedspeed;
                                this.dataGridView1.Rows[i].Cells[4].Value = cadence1;
                                this.dataGridView1.Rows[i].Cells[5].Value = altitude1;
                                this.dataGridView1.Rows[i].Cells[6].Value = power1;

                               
                                i = i + 1;
                                rows = rows + 1;

                                //calculate totals and averages
                                int rowhr = Int32.Parse(heartrate1);
                                double rowspeed = formattedspeed;
                                int rowaltitude = Int32.Parse(altitude1);
                                int rowpower = Int32.Parse(power1);

                                totspeed = totspeed + rowspeed;
                                tothr = tothr + rowhr;
                                totaltitude = totaltitude + rowaltitude;
                                totpower = totpower + rowpower;
                                
                                
                                //averages
                                avghr = tothr / rows;
                                avgspeed = totspeed / rows;
                                avgaltitude = totaltitude / rows;
                                avgpower = totpower / rows;

                                //calculate distance
                                minutespeed = avgspeed / 60;
                                secondspeed = minutespeed / 60;

                                int inthours = Int32.Parse(hours);
                                int intminutes = Int32.Parse(minutes);
                                double intseconds = Convert.ToDouble(seconds);
                                
                                hourdis = inthours * avgspeed;
                                mindis = intminutes * minutespeed;
                                secdis = intseconds * secondspeed;

                                totaldistance = hourdis + mindis + secdis;

                                //display as strings
                                String avghrstring = avghr.ToString();
                                label13.Text = avghrstring;

                                String avgspeedstring = avgspeed.ToString("#.##");
                                label9.Text = avgspeedstring + " KM/H";

                                String avgaltitudestring = avgaltitude.ToString();
                                label23.Text = avgaltitudestring;

                                String avgpowerstring = avgpower.ToString();
                                label19.Text = avgpowerstring;

                                String totaldisstring = totaldistance.ToString("#.##");
                                label7.Text = totaldisstring + " KM";

                                //get max
                                if (formattedspeed > maxspeedint)
                                {
                                    maxspeedint = formattedspeed;
                                    String maxspeedstring = maxspeedint.ToString("#.##");
                                    label11.Text = maxspeedstring + " KM/H";
                                }

                                if (rowhr > maxheartrateint)
                                {
                                    maxheartrateint = rowhr;
                                    String maxhrstring = maxheartrateint.ToString();
                                    label15.Text = maxhrstring;
                                }

                                if (rowpower > maxpowerint)
                                {
                                    maxpowerint = rowpower;
                                    String maxpowertring = maxpowerint.ToString();
                                    label21.Text = maxpowertring;
                                }

                                if (rowaltitude > maxaltitudeint)
                                {
                                    maxaltitudeint = rowaltitude;
                                    String maxaltitudestring = maxaltitudeint.ToString();
                                    label25.Text = maxaltitudestring;
                                }

                                //get min
                                if (rowhr >= 1)
                                {
                                    if (rowhr < minheartrateint)
                                    {
                                        minheartrateint = rowhr;
                                        String minhrstring = minheartrateint.ToString();
                                        label17.Text = minhrstring;
                                    }
                                }
                            }

                        }

                    }
                }
                catch (IOException e)
                {
                    Console.WriteLine(e);


            } 
            }







        }

        private void kmbutt_Click(object sender, EventArgs e)
        {

            String avgspeedstring = avgspeed.ToString("#.##");
            label9.Text = avgspeedstring + " KM/H";

            String maxspeedstring = maxspeedint.ToString("#.##");
            label11.Text = maxspeedstring + " KM/H";

            String totaldisstring = totaldistance.ToString("#.##");
            label7.Text = totaldisstring + " KM";
        }

        private void milesbutt_Click(object sender, EventArgs e)
        {
            double milesspeed = avgspeed * 1.6093;
            String milesspeedstring = milesspeed.ToString("#.##");
            label9.Text = milesspeedstring + " MPH";

            double milesdis = totaldistance * 1.6093;
            String totaldisstring = milesdis.ToString("#.##");
            label7.Text = totaldisstring + " Miles";

            double milesmaxspeed = maxspeedint * 1.6093;
            String maxspeedstring = milesmaxspeed.ToString("#.##");
            label11.Text = maxspeedstring + " MPH";

            int speedint = Int32.Parse(speed1);
            double formattedspeed = (double)speedint / 10;
            double speedinmiles = formattedspeed * 1.6093;
            this.dataGridView1.Rows[i].Cells[3].Value = speedinmiles;


        }
    }
}

